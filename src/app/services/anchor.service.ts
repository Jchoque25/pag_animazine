import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Anchor, Podcast } from '../interfaces/podcast.interface';


@Injectable({
  providedIn: 'root'
})
export class AnchorService {

  RssData: Anchor;
  podcasts : Podcast[] = []

  constructor(private http: HttpClient) { }

  getPodcast(){
    // tslint:disable-next-line: ban-types
    const requestOptions: Object = {
      observe: 'body',
      responseType: 'text'
    };
    let podcasts = this.http.get<any>('https://anchor.fm/s/c04e71c/podcast/rss', requestOptions);
    return podcasts;
  }
}
