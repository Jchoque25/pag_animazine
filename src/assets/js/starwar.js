var w = window.innerWidth;
var h = window.innerHeight;

var intro = document.getElementsByClassName("intro")[0];
var historia = document.getElementsByClassName("historia")[0];
var párrafos = document.getElementsByClassName("párrafos")[0];

intro.style.fontSize = w / 30 + "px";
historia.style.fontSize = w / 20 + "px";
párrafos.style.height = h + "px";

window.addEventListener("resize", function() {
  w = window.innerWidth;
  h = window.innerHeight;
  intro.style.fontSize = w / 30 + "px";
  historia.style.fontSize = w / 20 + "px";
  párrafos.style.height = h + "px";
});

function animar() {
  intro.className = 'intro texto_intro animación_intro';
  historia.className = 'historia texto_historia animación_historia';
}