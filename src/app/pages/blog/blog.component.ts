import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Blog } from 'src/app/interfaces/blog.interface';
import { FirestoreService } from 'src/app/services/firestore/firestore.service';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent{

  idBlog: string;
  blog: Blog;
  loaded = false;

  constructor(public serviceFire: FirestoreService, private route:ActivatedRoute) {
    this.route.params.subscribe( params => {
      this.idBlog = (params.id);
    });

    this.serviceFire.getItem('blogs',this.idBlog).subscribe((blog:Blog)=>{
      console.log(blog)
      this.blog = blog
    })

    setTimeout(() => {
      this.loaded = true;
    }, 500);
  }

}
