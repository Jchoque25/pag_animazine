import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { AlertModule } from 'ngx-bootstrap/alert';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { ModalModule } from 'ngx-bootstrap/modal';
import { JwBootstrapSwitchNg2Module } from 'jw-bootstrap-switch-ng2';
import { PopoverModule } from 'ngx-bootstrap/popover';

import { IndexComponent } from './index/index.component';
import { ProfilepageComponent } from './examples/profilepage/profilepage.component';
import { RegisterpageComponent } from './examples/registerpage/registerpage.component';
import { LandingpageComponent } from './examples/landingpage/landingpage.component';
import { NavComponent } from '../front/nav/nav.component';
import { FooterComponent } from '../front/footer/footer.component';
import { PlataformsComponent } from './plataforms/plataforms.component';
import { StarwarComponent } from './starwar/starwar.component';
import { InitialComponent } from './initial/initial.component';
import { ProfileComponent } from '../front/profile/profile.component';
import { ListenComponent } from './listen/listen.component';
import { PlayerComponent } from './player/player.component';
import { CardPodcastComponent } from '../front/card-podcast/card-podcast.component';
import { CardBlogComponent } from '../front/card-blog/card-blog.component';
import { AboutUsComponent } from '../front/about-us/about-us.component';
import { UsComponent } from './us/us.component';
import { BlogsComponent } from './blogs/blogs.component'
import { CookiesComponent } from '../front/cookies/cookies.component';
import { PoliticsComponent } from './politics/politics.component';
import { BlogComponent } from './blog/blog.component';
@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    RouterModule,
    BsDropdownModule.forRoot(),
    ProgressbarModule.forRoot(),
    TooltipModule.forRoot(),
    PopoverModule.forRoot(),
    CollapseModule.forRoot(),
    JwBootstrapSwitchNg2Module,
    TabsModule.forRoot(),
    PaginationModule.forRoot(),
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    CarouselModule.forRoot(),
    ModalModule.forRoot()
  ],
  declarations: [
    IndexComponent,
    ProfilepageComponent,
    RegisterpageComponent,
    LandingpageComponent,
    NavComponent,
    FooterComponent,
    PlataformsComponent,
    StarwarComponent,
    InitialComponent,
    ProfileComponent,
    ListenComponent,
    PlayerComponent,
    CardPodcastComponent,
    AboutUsComponent,
    UsComponent,
    CardBlogComponent,
    BlogsComponent,
    CookiesComponent,
    PoliticsComponent,
    BlogComponent
  ],
  exports: [
    IndexComponent,
    ProfilepageComponent,
    RegisterpageComponent,
    LandingpageComponent
  ],
  providers: []
})
export class PagesModule {}
