import { Component, OnInit, OnDestroy } from '@angular/core';
import { AnchorService } from '../../services/anchor.service';
import { Anchor } from '../../interfaces/podcast.interface';
import { Podcast } from '../../interfaces/podcast.interface';
import { FirestoreService } from 'src/app/services/firestore/firestore.service';
import { Blog } from 'src/app/interfaces/blog.interface';

import * as xml2js from 'xml2js';

@Component({
  selector: 'app-index',
  templateUrl: 'index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit, OnDestroy {

  RssData: Anchor;
  loaded = false;
  isCollapsed = true;
  date = new Date();
  pagination = 3;
  pagination1 = 1;
  podcasts : Podcast[] = []
  blogs: Blog[] = [];

  constructor(public anchorService: AnchorService, public serviceFire: FirestoreService) {
    // this.serviceFire.getItems('blogs').subscribe((blogsSnapshot:Blog[]) => {
    //   this.blogs = blogsSnapshot.filter((item, index) => index < 2 )
    // });
  }

  loadPodcast(){
    const pods = this.anchorService.getPodcast().subscribe(data => {
      const parseString = xml2js.parseString;
      parseString(data, (err, result: Anchor) => {
        this.RssData = result;
        this.RssData.rss.channel[0].item.slice(0, 3).map(pod => {
          switch (pod['itunes:season'][0]) {
            case '1':
              pod.color = 'warning';
              break;
            case '2':
              pod.color = 'info';
              break;
            default:
              pod.color = 'warning';
              break;
          }
          this.podcasts.push(pod);
        })
      });
    });
    this.loaded = true;
  }

  scrollToDownload(element: any) {
    element.scrollIntoView({ behavior: 'smooth' });
  }

  ngOnInit() {
    setTimeout(() => {
      this.loadPodcast()
    }, 1500);
  }

  ngOnDestroy() {
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('index-page');
  }
}