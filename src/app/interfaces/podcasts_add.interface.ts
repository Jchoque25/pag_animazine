export interface PodcastsAdd {
    image: string;
    likes: number;
    id?: string;
}