import { Component, OnInit } from '@angular/core';
import { Podcast } from 'src/app/interfaces/podcast.interface';
import { PodcastsAdd } from 'src/app/interfaces/podcasts_add.interface';
import { AnchorService } from 'src/app/services/anchor.service';
import { FirestoreService } from 'src/app/services/firestore/firestore.service';
import { ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';
import { Anchor } from '../../interfaces/podcast.interface';

import * as xml2js from 'xml2js';

@Component({
  selector: 'app-listen',
  templateUrl: './listen.component.html',
  styleUrls: ['./listen.component.scss']
})

export class ListenComponent implements OnInit{

  RssData: Anchor;
  podcasts : Podcast[] = []
  podcastAdd: PodcastsAdd[] = [];
  loaded = false;
  idPod = 0
  baseUrl = environment.baseUrl

  constructor(public anchorService: AnchorService, public serviceFire: FirestoreService, private route:ActivatedRoute) {
    this.route.params.subscribe( params => {
      this.idPod = (params.id);
    });

    setTimeout(() => {
      this.loaded = true;
    }, 3000);

    if (this.idPod > 0){
      setTimeout(() => {
        const el = document.getElementById(this.idPod.toString())
        el.scrollIntoView(true);
        window.scrollBy(0, -80);
      }, 1500);
    }
  }

  ngOnInit(): void {
    this.loadPodcast()
  }

  loadPodcast(){
    const pods = this.anchorService.getPodcast().subscribe(data => {
      const parseString = xml2js.parseString;
      parseString(data, (err, result: Anchor) => {
        this.RssData = result;
        this.RssData.rss.channel[0].item.map(pod => {
          switch (pod['itunes:season'][0]) {
            case '1':
              pod.color = 'warning';
              break;
            case '2':
              pod.color = 'info';
              break;
            default:
              pod.color = 'warning';
              break;
          }
          this.podcasts.push(pod);
        })
      });
    });
  }
}
