export interface Profiles{
    name: string;
    description: string;
    image: string;
    films: Array<string>;
    anime: Array<string>;
    series: Array<string>;
    social: Array<string>;
    birthday: string;
    id: string;
}