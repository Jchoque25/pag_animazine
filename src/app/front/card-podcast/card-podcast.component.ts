import { Component, OnInit, Input } from '@angular/core';
import { PlayerComponent } from '../../pages/player/player.component';
import { PodcastsAdd } from '../../interfaces/podcasts_add.interface';
import { FirestoreService } from '../../services/firestore/firestore.service';
@Component({
  selector: 'app-card-podcast',
  templateUrl: './card-podcast.component.html',
  styleUrls: ['./card-podcast.component.scss']
})
export class CardPodcastComponent implements OnInit {

  @Input() items: any[] = [];
  @Input() idPod: string = '';

  loading = false;
  podcast : any;

  constructor(public playerComponent: PlayerComponent) { }

  ngOnInit(): void {
  }

  receiveMessage($event) {
    this.loading = false;
  }

  listening(pod: any){
    this.loading = false;

    setTimeout(() => {
      this.podcast = pod;
      this.loading = true;
    }, 700);
  }

  getImageSeason(season: String){
    return "./assets/img/seasons/"+season+".jpg"
  }
}
