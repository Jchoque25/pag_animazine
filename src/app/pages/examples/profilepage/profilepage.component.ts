import { Component, OnInit, OnDestroy } from '@angular/core';
import { FirestoreService } from 'src/app/services/firestore/firestore.service';
import { Profiles } from 'src/app/interfaces/profiles.interface';
import { ActivatedRoute } from '@angular/router';
import { ServerService } from '../../../services/server.service';

@Component({
  selector: 'app-profilepage',
  templateUrl: 'profilepage.component.html',
  styleUrls: ['./profilepage.component.scss']
})
export class ProfilepageComponent implements OnInit, OnDestroy {
  isCollapsed = true;

  MyProfile: Profiles;
  MyId: string;
  loaded = false;

  constructor(public serviceFire: FirestoreService, private route:ActivatedRoute) {
    this.route.params.subscribe( params => {
      this.MyId=(params.id);
    });
    this.serviceFire.getItems('profiles').subscribe((data_profile: Profiles[])=>{
      data_profile.forEach(profile => {
        if (profile.id === this.MyId){
          this.MyProfile = profile
        }
      })
    })
  }

  ngOnInit() {
    setTimeout(() => {
      this.loaded = true;
    }, 500);
    var body = document.getElementsByTagName('body')[0];
    body.classList.add('profile-page');
  }
  ngOnDestroy() {
    var body = document.getElementsByTagName('body')[0];
    body.classList.remove('profile-page');
  }
}
