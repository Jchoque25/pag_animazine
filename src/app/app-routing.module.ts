import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { IndexComponent } from './pages/index/index.component';
import { ProfilepageComponent } from './pages/examples/profilepage/profilepage.component';
import { RegisterpageComponent } from './pages/examples/registerpage/registerpage.component';
import { LandingpageComponent } from './pages/examples/landingpage/landingpage.component';
import { InitialComponent } from './pages/initial/initial.component';
import { ListenComponent } from './pages/listen/listen.component';
import { UsComponent } from './pages/us/us.component';
import { BlogsComponent } from './pages/blogs/blogs.component';
import { PoliticsComponent } from './pages/politics/politics.component';
import { BlogComponent } from './pages/blog/blog.component';

const routes: Routes = [
  { path: '', redirectTo: 'initial', pathMatch: 'full', data: {animation: 'Initial'}  },
  { path: 'initial', component: InitialComponent, data: {animation: 'Initial'}  },
  { path: 'home', component: IndexComponent, data: {animation: 'Home'} },
  { path: 'profile/:id', component: ProfilepageComponent },
  { path: 'register', component: RegisterpageComponent },
  { path: 'initial', component: InitialComponent },
  { path: 'landing', component: LandingpageComponent },
  { path: 'listen', component: ListenComponent },
  { path: 'listen/:id', component: ListenComponent },
  { path: 'us', component: UsComponent },
  // { path: 'blogs', component: BlogsComponent},
  { path: 'cookies', component: PoliticsComponent},
  // { path: 'blog/:id', component: BlogComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'initial' }
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes, {
      useHash: true,
      onSameUrlNavigation: 'ignore',
      anchorScrolling:'enabled',
      scrollPositionRestoration: 'enabled'
    })
  ],
  exports: []
})
export class AppRoutingModule {}
