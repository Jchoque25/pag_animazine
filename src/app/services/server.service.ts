import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { Anchor, Podcast } from '../interfaces/podcast.interface';
import { Blog } from 'src/app/interfaces/blog.interface';

const httpOptions = {
  headers: new HttpHeaders({
    'Access-Control-Allow-Origin':'*'
  })
};

@Injectable({
  providedIn: 'root'
})
export class ServerService {

  RssData: Anchor;
  blogs : Blog[] = [];

  constructor(private http: HttpClient) { }

  getProfiles(){
    this.http.get<any>('http://localhost:8080/profiles', httpOptions).subscribe(data => {
      console.log(data);
    })
  }

  getBlogs(){
    this.http.get<any>('http://localhost:8080/blogs', httpOptions).subscribe(data => {
      console.log(data);
    })
  }
}
