import { Component, OnInit } from '@angular/core';
import { FirestoreService } from 'src/app/services/firestore/firestore.service';
import { Blog } from 'src/app/interfaces/blog.interface';
import { ServerService } from '../../services/server.service';

@Component({
  selector: 'app-blog',
  templateUrl: './blogs.component.html',
  styleUrls: ['./blogs.component.scss']
})
export class BlogsComponent implements OnInit {

  loaded = true;
  blogs: Blog[] = [];

  constructor(public serviceFire: FirestoreService, public ss: ServerService) {
    //this.ss.getBlogs();
    this.serviceFire.getItems('blogs').subscribe((blogsSnapshot:Blog[]) => {
      this.blogs = blogsSnapshot;
    });
  }

  ngOnInit(): void {
  }

}
