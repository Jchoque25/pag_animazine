import { Component, OnInit, Injectable, Input, OnDestroy, EventEmitter, Output } from '@angular/core';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss']
})
@Injectable({
  providedIn: 'root'
})

export class PlayerComponent implements OnInit, OnDestroy {

  @Input() podcast: any;
  @Output() messageEvent = new EventEmitter<boolean>();

  audio: any;
  audioTotalTime = 0;
  activeAudio = false;
  open = false;
  activeVolume = true;
  range = 0;
  current = '00:00';
  duration = '00:00';
  maximized = true;
  shared = 0
  baseUrl = environment.baseUrl

  constructor() { }

  ngOnInit(): void {
    this.audio = new Audio();
    this.audio.src = this.podcast.enclosure[0].$.url;
    this.audio.load();
    this.play();
  }

  play() {
    if (!this.activeAudio) {
      this.audio.play();
      this.activeAudio = true;
      this.open = true;
      this.duration = '00:00';
      setTimeout(() => {
        this.audio.addEventListener('timeupdate', this.calculateTime, false);
      }, 1000);
    }else{
      this.audio.pause();
      this.activeAudio = false;
    }
  }

  reset(){
    this.activeAudio = !this.activeAudio;

    this.audio.pause();
    this.audio.currentTime = 0;
    this.play();
  }

  volume(){
    const bool = this.audio.volume;

    if(bool === 0){
      this.audio.volume = 1;
      this.activeVolume = true;
    }else{
      this.audio.volume = 0;
      this.activeVolume = false;
    }
  }

  changeTime(value: number){
    this.audio.pause();
    this.audio.currentTime = value;
    this.audio.play();

    this.updateCurrentTime(this.audio.currentTime)
  }

  fadeOut(){
    setTimeout(() => {
      this.maximized = false;
    }, 500);
  }

  fadeIn(){
    setTimeout(() => {
      this.maximized = true;
    }, 500);
  }

  destroy(){
    if(this.audio) {
      this.activeAudio = false;
      this.open = false;
      this.audio.pause();
      setTimeout(() => {
        this.messageEvent.emit(false)
      }, 800);
    }
  }

  sharedLink(social, name){
    let url = ''
    const sharedUrl = this.baseUrl+'#/listen/'+name;
    switch (social) {
      case 'facebook':
        url = 'https://www.facebook.com/sharer.php?u='+sharedUrl
        break;
      case 'instagram':
        url = 'https://www.facebook.com/sharer.php?u=[post-url]'
        break;
      case 'twitter':
        url = 'https://twitter.com/intent/tweet/?text=Escucha nuestro podcast'+this.shared+' y visitanos en '+sharedUrl
        break;
      case 'whatsapp':
        url = 'https://api.whatsapp.com/send?text='+sharedUrl
        break;
      default:
        break;
    }
    window.open(url,'test','height=400,width=550');
  }

  copy(name){
    document.addEventListener('copy', (e: ClipboardEvent) => {
      e.clipboardData.setData('text/plain', (this.baseUrl+'#/listen/'+name));
      e.preventDefault();
      document.removeEventListener('copy', null);
    });
    document.execCommand('copy');

    Swal.fire({
      position: 'center',
      title: '',
      html: 'Copiado',
      showConfirmButton: false,
      timer: 1500,
      width: 200
    })
  }

  private calculateTime = (evt) => {
    const ct = this.audio.currentTime;
    const d = this.audio.duration;

    const minute = (Math.floor(d / 60));
    const second = (Math.floor(d % 60));
    this.duration = minute + ':' + second;

    this.updateCurrentTime(ct)
    this.setTimeElapsed(ct)
  }

  private updateCurrentTime(current){
      this.range = current;
  }

  private setTimeElapsed(ct: number): void {
    const seconds     = Math.floor(ct % 60);
    const displaySecs = (seconds < 10) ? '0' + seconds : seconds;
    const minutes     = Math.floor((ct / 60) % 60);
    const displayMins = (minutes < 10) ? '0' + minutes : minutes;

    this.current = displayMins + ':' + displaySecs;
  }

  ngOnDestroy() {
    this.destroy();
  }
}


