import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  @ViewChild("footer") divView: ElementRef;
  color = 'red';

  constructor() { }

  ngOnInit(): void {
  }

  changeBg(st){
    this.divView.nativeElement.classList.remove('default_bg');
    this.divView.nativeElement.classList.remove('twitter_bg');
    this.divView.nativeElement.classList.remove('facebook_bg');
    this.divView.nativeElement.classList.remove('instagram_bg');
    this.divView.nativeElement.classList.remove('youtube_bg');

    let bg = 'default_bg';
    console.log(st);
    switch (st) {
      case 1:
        bg = 'twitter_bg';
        break;
      case 2:
        bg = 'instagram_bg';
        break;
      case 3:
        bg = 'facebook_bg';
        break;
      case 4:
        bg = 'youtube_bg';
        break;
      case 5:
        bg = 'default_bg'
      default:
        break;
    }
    this.divView.nativeElement.classList.add(bg);
  }

}
