export interface Blog {
    title: string;
    description: string;
    autor: string;
    date: string;
    category: string;
    image?: string;
    id: string;
    star: string;
}