import { Component, OnInit } from '@angular/core';
import { FirestoreService } from 'src/app/services/firestore/firestore.service';
import { Profiles } from 'src/app/interfaces/profiles.interface';
import { Directive, ElementRef } from '@angular/core';

@Component({
  selector: 'app-us',
  templateUrl: './us.component.html',
  styleUrls: ['./us.component.scss']
})

@Directive({ selector: 'img' })

export class UsComponent implements OnInit {
  loaded = false;

  constructor({ nativeElement }: ElementRef<HTMLImageElement>) {
    const supports = 'loading' in HTMLImageElement.prototype;

    if (supports) {
      console.log("yes")
      nativeElement.setAttribute('loading', 'lazy');
    } else {
      console.log("UNAO")
    }
  }
  ngOnInit() {
    let body = document.getElementsByTagName('body')[0];
    body.classList.add('profile-page');

    setTimeout(() => {
      this.loaded = true;
    }, 1000);
  }
  ngOnDestroy() {
    let body = document.getElementsByTagName('body')[0];
    body.classList.remove('profile-page');
  }
}
