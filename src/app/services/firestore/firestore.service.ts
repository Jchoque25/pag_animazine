import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class FirestoreService {

  constructor(private fsDB: AngularFirestore) { }

  public createItem(coll: string, data: {nombre: string, url: string}) {
    return this.fsDB.collection(coll).add(data);
  }

  public getItem(coll: string, documentId: string) {
    return this.fsDB.collection(coll).doc(documentId).valueChanges();
  }

  public getItems(coll: string) : Observable<any>{
    return this.fsDB.collection(coll).snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
            const data = a.payload.doc.data();
            const id = a.payload.doc.id;
            data['id'] = id

            return data
        });
      })
  );
  }

  public updateItem(coll: string, documentId: string, data: any) {
    return this.fsDB.collection(coll).doc(documentId).set(data);
  }
}
