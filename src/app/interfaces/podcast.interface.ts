import { PodcastsAdd } from './podcasts_add.interface';
export interface Anchor {
    rss: IRssObject;
}

export interface IRssObject {
    $: any;
    channel: Array<IRssChannel>;
}

export interface IRssChannel {
    'atom:link': Array<string>;
    description: Array<string>;
    image: Array<IRssImage>;
    item: Array<Podcast>;
    language: Array<string>;
    lastBuildDate: Date;
    link: Array<string>;
    title: Array<string>;
}

export interface IRssImage {
    link: Array<string>;
    title: Array<string>;
    url: Array<string>;
}

export interface Podcast {
    color: string;
    category: Array<string>;
    description: Array<string>;
    guid: any;
    enclosure: any;
    link: Array<string>;
    pubDate: Date;
    info:PodcastsAdd;
    title: Array<string>;
}