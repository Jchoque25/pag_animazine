import { Component, OnInit, Input } from '@angular/core';
import { Blog } from 'src/app/interfaces/blog.interface';
import { Router } from '@angular/router';

@Component({
  selector: 'app-card-blog',
  templateUrl: './card-blog.component.html',
  styleUrls: ['./card-blog.component.scss']
})
export class CardBlogComponent implements OnInit {

  @Input() blogs: Blog[] = [];

  loading = true;
  filteredBlog: Blog[] = []
  filter = ''

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  filterFunction(blogs: Blog[]){
    if (this.filter !== ''){
      return this.blogs.filter(i => i.category === this.filter);
    }else{
      return this.blogs
    }
  }

  viewBlog(blog: Blog){
    const blogId = blog.id

    this.router.navigate(['/blog', blogId]);
  }
}
