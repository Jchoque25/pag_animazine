import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cookies',
  templateUrl: './cookies.component.html',
  styleUrls: ['./cookies.component.scss']
})
export class CookiesComponent implements OnInit {

  aceptedCookies = false;

  constructor() { }

  ngOnInit(): void {
    this.compruebaAceptaCookies();
  }

  aceptarCookies() {
    localStorage.aceptaCookies = 'true';
    this.aceptedCookies = true;
  }

  compruebaAceptaCookies() {
    if(localStorage.aceptaCookies === 'true'){
      this.aceptedCookies = true;
    }
  }
}
