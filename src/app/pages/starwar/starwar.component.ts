import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-starwar',
  templateUrl: './starwar.component.html',
  styleUrls: ['./starwar.component.scss']
})
export class StarwarComponent implements OnInit {

  @ViewChild('audioOption') audioPlayerRef: ElementRef;

  activeClass = false;
  constructor() { }

  ngOnInit(): void {
  }

  animar(){
    this.activeClass = true;
    this.audioPlayerRef.nativeElement.play();
  }
}
